<!DOCTYPE html>
<head><title>Slideshow</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
<style>
#slideshow{height:330px; width:400px; float:left}
#slides{height:300px; position:relative}.slide{height:300px; width:400px; overflow:hidden; position:absolute; background:#000; color:#fff}
#slides-controls{width:60px; background-color:#fff; height:20px; margin-left:auto; margin-right:auto; padding:5px}
#slides-controls a{margin:5px; width:8px; height:8px; border-radius:4px; -moz-border-radius:4px; -webkit-border-radius:4px; -o-border-radius:4px; background:#F60; border:1px solid #fff; text-indent:-9000px; display:block; overflow:hidden; float:left}
#slides-controls a:hover{background:#F30}#slides-controls a.highlight{background:#fff; border:1px solid #F30}
</style>
<script src="slider.js" type="text/javascript"></script>
</head>

<body onload="setUpSlideShow()">
<div id="slideshow">
<div id="slides">
<?php
				$dir = new DirectoryIterator(dirname(__FILE__)."/news");
				foreach ($dir as $fileinfo) {				
  				  	if (!$fileinfo->isDot()) {
					$i++;
					echo "\n<div class=\"slide\">";
					$homepage = file_get_contents(dirname(__FILE__)."/news/$fileinfo");
					echo utf8_decode($homepage);
					//require_once(dirname(__FILE__)."/news/$fileinfo");
        				//var_dump($fileinfo->getFilename());
					echo "</div>";
    					}
				}
				?>
</div>
<div id="slides-controls"></a>
<?php	for ($j=1;$j<$i+1;$j++) {echo "<a href=\"#\">$j</a>";}		?>
</div>
</div>
</body>
</html>
