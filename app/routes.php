<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::resource('posts', 'PostsController');
Route::get('admin', array('as' => 'posts.admin', function()
{
	return View::make('posts.admin');
}));

Route::get('/', function()
{
	return View::make('hello');
});
