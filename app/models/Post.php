<?php

class Post extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['title', 'text', 'published_at', 'expired_at'];


	public function getDates()
	{
	    return array('created_at', 'updated_at', 'published_at', 'expired_at');
	}

}