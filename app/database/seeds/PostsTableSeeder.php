<?php


class PostsTableSeeder extends Seeder {

	public function run()
	{

		Post::create([
			'title'=>'Studienämnden informerar',
			'text'=> '
				<p>Kursen <strong>Ingenjörens roll i arbetslivet</strong> är under utveckling. Det gamla teorimomentet förminskas och ett nytt intressant moment införs! </p>
				<p>Institutionen för Fysik arbetar med att utveckla rutiner kring laborationer så att det ska finnas tydliga regler och rutiner för både handledare och studenter. </p>
			', 
			'published_at' => Carbon::now(),
			'expired_at' => Carbon::now()->addWeeks(2)
		]);

		Post::create([
			'title'=>'Studienämnden rekryterar',
			'text'=> '<p>
				Att vara med i studienämnden innebär att man är en del av utvecklingsarbetet på Teknisk Fysik och man får stor möjlighet att diskutera och påverka problem och utvecklingsmöjligheter för programmet (det är dessutom ordentligt roligt). Är du intresserad? Kontakta Jonatan! </p>
				<br>
				<p>
				Kvalitetsamanuens<br> 
				Jonatan Mossegård <br>
				<a href="">kvalitet@tekniskfysik.se</a>
				</p> '
			, 
			'published_at' => Carbon::now(),
			'expired_at' => Carbon::now()->addWeeks(2)
		]);


	}

}