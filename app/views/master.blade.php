<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>@yield('title', 'Presentation')</title>

		@section('styles')
		    {{ HTML::style('css/all.css') }}
		@show

		@section('scripts')
			{{HTML::script(url("//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"))}}
			{{HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js')}}
			{{HTML::script(url("js/bootswatch.js"))}}
		@show

		@section('angular')	
		@show
	</head>

	@section('body')
		<body class="container">
			@include('navbar')
			<div class="panel panel-default">
		  		<div class="panel-body">
		    		@yield('content')
		  		</div>
			</div>
		</body>
	@show

</html>
	