@extends('master')
@section('title', 'Redigera Poster')

@section('content')
	<table class="table table-striped table-bordered">
		@foreach (Post::all() as $index => $post)
		 <tr><td>{{$post->title}} </td>
		 	<td>{{link_to_route('posts.show', 'Visa', [$post->id])}}</td>
		 	<td>{{link_to_route('posts.edit', 'Redigera', [$post->id])}}</td>
		</tr>
		@endforeach
	</table>

	<div>
		{{ link_to_route('posts.create', "Lägg till post", [], ['class' => 'btn btn-primary pull-right']) }}
	</div>
@stop

