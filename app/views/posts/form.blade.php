<div class="form-group">
	{{ Form::label('title', 'Titel') }}
	{{ Form::text('title', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('text', 'Text') }}
	{{ Form::textarea('text', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('published_at', 'Publish') }}
	{{ Form::text('published_at', Carbon::now(), array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('expired_at', 'Expire') }}
	{{ Form::text('expired_at', Carbon::now()->addWeeks(2), array('class' => 'form-control')) }}
</div>