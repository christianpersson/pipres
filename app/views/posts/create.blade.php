@extends('master')
@section('content')
	{{ Form::open(['url' => 'posts', 'files' => true, 'method' => 'post']) }}

	@include('posts.form')

	{{ Form::submit('Skapa', array('class' => 'btn btn-primary')) }}


	{{ Form::close() }}
@stop