<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>@yield('title', 'Presentation')</title>

    {{ HTML::style('css/all.css') }}
    {{ HTML::style('css/pres.css') }}

    {{HTML::script(url("//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"))}}
    {{HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js')}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myCarousel').carousel({
                pause: 'none',
                interval: 3000
            })
        });
    </script>

</head>


<body>

<div id="background-carousel">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner text-center">
            <div class="item cal">
                <div class="container2">
                    <h1>Kalender</h1>

                    <div class="well"><? require_once('mainCalendar.php'); ?></div>
                </div>
                <div style="position: absolute; bottom: 40px; left: 0; width:100%"><p style="text-align:center">Tips!
                        Synka din kalender från tekniskfysik.se/kalender</div>
            </div>
            @foreach (Post::all() as $index => $post)
            <div class="{{$index == 0 ? " item active
            ":"item"}}" id="content-wrapper">
            <div class="container2">
                <h1>{{$post->title}}</h1>

                <div class="well">{{$post->text}}</div>
            </div>
        </div>
        @endforeach

    </div>
</div>
</div>

</body>

</html>