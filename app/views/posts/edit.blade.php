@extends('master')
@section('title', 'Uppdatera -' . $post->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($post, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('posts.update', $post->id)))}}
		@include('posts.form')
		{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}

	{{Form::open(['url' => route('posts.destroy', [$post->id]), 'method' => 'delete'])}}
		{{ Form::submit('Ta bort', array('class' => 'btn btn-primary')) }}	
	{{Form::close()}}
@stop