<div class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="">PiPresentation</a>
  </div>
  <div class="navbar-collapse collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav">
      <li>{{link_to_route('posts.index', 'Presentation')}}</li>
      <li>{{link_to_route('posts.admin', 'Administrering')}}</li>
      <li>{{link_to_route('posts.create', 'Lägg till ny')}}</li>
    </ul>
  </div>
</div>